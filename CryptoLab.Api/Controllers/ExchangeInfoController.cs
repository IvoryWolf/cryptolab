﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Intrinsics;
using System.Threading.Tasks;
using BinanceDotNetApi;
using BinanceModel;
using Microsoft.AspNetCore.Mvc;

namespace CryptoLab.Api.Controllers
{
    [ApiController]
    [Route("api/v1/exchangeinfo")]
    public class ExchangeInfoController : ControllerBase
    {

        private readonly BinanceDotNetApi.BinanceDotNet _binanceDotNet;
        public ExchangeInfoController(BinanceDotNet binanceDotNet)
        {
            _binanceDotNet = binanceDotNet;
        }

        [HttpGet]
        public async Task<IActionResult> GetExchangeInfo()
        {
            return Ok(await _binanceDotNet.ExchangeInfo());
        }

    }
}
